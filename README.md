# Disable Trackpad script

A simple shell script to disable synaptic touchpads on thinkpad devices running linux or bsd, with added notifications.
The `trackpointspeed` script changes the sensitivity of the trackpoint, it's optional, but its a good addition.

The script only requires the `xinput` package to work, any notification service with `notify-send` will also work if you want notification support
